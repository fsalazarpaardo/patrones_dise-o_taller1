# Patrones de Diseño

# Taller 1 - Librería de notificaciones.

# Enunciado del ejercicio


*  Suponga que usted trabaja en una empresa de desarrollo y le asignaron la tarea de modificar una librería de notificación que le permite a otros programas notificar a sus usuarios acerca de eventos importantes.
La versión inicial de la librería estaba basada en la clase Notificador, la cual tiene un método enviar. Este método puede aceptar un mensaje como parámetro de un cliente y enviar el mensaje a una lista de correos que fueron pasados al Notificador vía su constructor. Una aplicación de un tercero, que actúa como cliente, es la que debe crear y configurar el objeto Notificador una vez y luego los usa cada vez que algo importante ocurre.
En algún punto, usted se da cuenta que los usuarios de la librería esperan no solo correos de notificación. Muchos de ellos requieren recibir SMS, otros quieren recibir notificaciones vía Facebook y los clientes corporativos quizá quieran recibir notificaciones vía el sistema de mensajería empresarial. Modifique el diseño para esta situación, considerando que pueden ser muchas más opciones de notificación de las mencionadas.
Más adelante algún cliente pregunta: ¿por qué no se pueden usar varios tipos de notificación al mismo tiempo? A usted le piden que modifique nuevamente el diseño para considerar dicha situación, tratando de reutilizar lo que se pueda y dejando el diseño flexible para poder incorporar nuevas opciones.
# Objetivos del taller

*  Realizar un modelo de diseño del enunciado dado que considere buenas prácticas de diseño, utilizando un diagrama de clases de UML.

*  Implementar el modelo de diseño planteado, utilizando el lenguaje de programación que soporte orientación por objetos.


*  Identificar los patrones de diseño utilizado en el modelo.
# Aspectos a tener en cuenta

*  El modelo de diseño final debe tener el mayor detalle posible.


*  El código debe estar organizado y comentado en los lugares que se requiera explicación.

*  No se deben considerar en el modelo los elementos de la interfaz de usuario. Sin embargo, opcionalmente se puede implementar la interfaz gráfica para el ejercicio.

*  Todo se debe entregar en un solo archivo (.zip o .rar) cuyo nombre será el nombre de los integrantes del grupo separados por _.
# Entregables
Se debe entregar un documento que contenga los siguientes elementos:

*  Análisis del problema, teniendo en cuenta la justificación de todas las decisiones tomadas en el diseño.

*  Diagrama de clases UML con el diseño de la solución (se recomienda presentar el proceso de construcción del diagrama para ir ilustrando las decisiones tomadas)


*  Identificación y descripción de los patrones que se ajustan al problema y la solución planteada, justificando adecuadamente la respuesta en términos de los propósitos del patrón y describiendo los roles más relevantes de los elementos del modelo en el patrón.

*  Igualmente se debe entregar el código fuente con la implementación de la solución. Dicha implementación no tiene que ser totalmente funcional; la idea es que evidencia el código que es relevante para el diseño planteado.

